@REM This is a wrapper for running command line programs or batch files from
@REM within a c# Process. The only reason this really exists is to be able to 
@REM put a PAUSe in, so I can see errors, etc when executing.

@REM The "CALL" command is used to call other batch files within a batch file, 
@REM without aborting the execution of the calling batch file, and using the 
@REM same environment for both batch files. Essentially, without the "CALL"
@REM statement, the batch file you are calling could close the window without
@REM ever reaching our PAUSE statement. Here's some more info:
@REM http://stackoverflow.com/questions/4666045/batch-file-command-pause-does-not-work
@REM http://www.robvanderwoude.com/call.php

@ECHO OFF

CALL %*

PAUSE