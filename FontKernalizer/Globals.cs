﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Globals.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FontKernalizer
{
    using System;
    using System.Linq;

    /// <summary>
    /// Class definition for Globals.cs.
    /// </summary>
    public static class Globals
    {
        /// <summary>
        /// When displaying a graphical timer, I always want to show the
        /// minutes and seconds, but I don't want to show the hours unless
        /// they are necessary. This isn't possible using a single format
        /// string as far as I can tell, so I have to use two.
        /// Examples:
        /// 00:15       (15 secs - don't show hours)
        /// 10:15       (10 min, 15 secs - don't show hours)
        /// 01:14:22    (1 hour, 14 min, 22 secs)
        /// </summary>
        public const string LessThanOneHourDateFormat = "{0:mm\\:ss}";
        public const string MoreThanOneHourDateFormat = "{0:hh\\:mm\\:ss}";

        /// <summary>
        /// A delay used to ensure the user gets to see graphical status 
        /// messages before they dissapear.
        /// </summary>
        public const int ReadingDelay = 500;
        public const string OutputKerningDataFileName = "kerning_data.ssbgame";

        /// <summary>
        /// The list of valid font types. 
        /// </summary>
        public static readonly string[] ValidFontExtensions = { "*.ttf", "*.otf" };
    }
}
