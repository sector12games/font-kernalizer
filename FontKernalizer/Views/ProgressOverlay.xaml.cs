﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgressOverlay.xaml.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FontKernalizer.Views
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Threading;
    using FontKernalizer.KerningLogic;
    using FontKernalizer.ViewModels;
    using Sector12Common.Controls.ControlData;
    using Sector12Common.Helpers;

    /// <summary>
    /// Interaction logic for ProgressOverlay.xaml
    /// </summary>
    public partial class ProgressOverlay : UserControl
    {
        private DispatcherTimer _timer;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressOverlay"/> class.
        /// </summary>
        public ProgressOverlay()
        {
            InitializeComponent();

            // Setup our ProgressBars for use from a worker thread. This way, we
            // can update our progress bars without having to make sure every
            // call we make is executed on the dispatcher thread.
            ProgressBars.InitializeForUseInWorkerThread(Dispatcher, Globals.ReadingDelay);

            // Set up our time elapsed timer.
            _timer = new DispatcherTimer();
            _timer.Interval = TimeSpan.FromMilliseconds(1000);
            _timer.Tick += Timer_Tick;
        }

        /// <summary>
        /// Gets or sets our primary ViewModel. Ties data to the front end via binding.
        /// </summary>
        public MainWindowViewModel MainWindowViewModel { get; set; }

        /// <summary>
        /// Starts executing the build. This method should be called from within
        /// a separate thread.
        /// </summary>
        /// <param name="afdko"></param>
        /// <param name="target"></param>
        /// <param name="font"></param>
        public void StartWorking(DirectoryInfo afdko, DirectoryInfo target, FileInfo font)
        {
            // Reset everything and start timers, etc.
            UI(() => BuildStarted());

            // Populate our progress bars with tasks
            UI(() => InitializeProgressBars());

            // Go, Jerry, Go!
            UI(() => JerryImage.Play());

            // Run the ttx.exe dump
            FileInfo ttxFile = null;
            ExecuteTask(() => ttxFile = TtxBuilder.RunTtx(
                afdko, 
                font, 
                MainWindowViewModel.HideCommandWindow, 
                MainWindowViewModel.Overwrite));

            // Parse the xml data
            KerningData data = null;
            try
            {
                ExecuteTask(() => data = TtxParser.ParseGposTable(ttxFile));
            }
            catch (Exception)
            {
                // The build failed. Terminate and display an error message.
                UI(() => BuildCompleted(data, false));
                return;
            }

            // If there were any errors parsing the data, the build has failed.
            // Not all font files are supported.
            if (data != null)
            {
                // Map the adobe glyph strings provided by the .ttx dump to unicode values
                if (MainWindowViewModel.MapUsingAGLFN)
                    ExecuteTask(() => AdobeGlyphStringToUnicodeMapper.MapAGLFN(data));
                else
                    ExecuteTask(() => AdobeGlyphStringToUnicodeMapper.MapAGL(data));

                // Serialize our final c# object out to file
                ExecuteTask(
                    () => SerializationHelper.SerializeObjectToFile(
                        data,
                        target.FullName + "\\" + Globals.OutputKerningDataFileName,
                        MainWindowViewModel.Overwrite));

                // Delete the ttx file if the "Keep ttx data" flag was not set
                if (!MainWindowViewModel.KeepTtxXmlOutput)
                    FileSystemHelper.DeleteFileWaitForAccess(ttxFile);

                // Finish our build
                UI(() => BuildCompleted(data, true));
            }
        }

        /// <summary>
        /// Should be called before the build begins. This resets all progress bar
        /// and view model values, starts our timers, etc.
        /// </summary>
        private void BuildStarted()
        {
            // Reset everything
            MainWindowViewModel.Reset();
            ProgressBars.Reset(true);

            // Jerry can start humping
            JerryImage.Play();

            // Start our timers
            StartTimers();
            MainWindowViewModel.IsExecuting = true;
        }

        /// <summary>
        /// Populates our progress bars with tasks.
        /// </summary>
        private void InitializeProgressBars()
        {
            // Add our tasks
            ProgressBars.Tasks.Add(new ProgressBarTask("Running ttx.exe on font file...", "", 0));
            ProgressBars.Tasks.Add(new ProgressBarTask("Parsing GPOS table...", "", 0));
            ProgressBars.Tasks.Add(new ProgressBarTask("Mapping glyph strings to unicode...", "", 0));
            ProgressBars.Tasks.Add(new ProgressBarTask("Serializing kerning data...", "", 0));

            // Show our progress bars
            UI(() => ProgressBars.Visibility = Visibility.Visible);
        }

        /// <summary>
        /// Executes a single task and calls the NextTask() method on the progress
        /// bars control. This method will also check for any pauses or aborts before
        /// executing the given task.
        /// </summary>
        /// <param name="methodToExecute"></param>
        /// <param name="subtaskName"></param>
        /// <param name="subtaskDetails"></param>
        private void ExecuteTask(Action methodToExecute, string subtaskName = "", string subtaskDetails = "")
        {
            // Update the progress bars
            ProgressBars.NextTask(subtaskName, subtaskDetails);

            // Execute the method
            methodToExecute();

            // For debugging
            //ProgressBars.PrintTotalTaskDebugInfo();
        }

        /// <summary>
        /// Code that we always want to run after our build has completed or
        /// been aborted.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="success"></param>
        private void BuildCompleted(KerningData data, bool success)
        {
            // Jerry can stop humping now
            UI(() => JerryImage.Pause());

            // Hide our progress bars
            UI(() => ProgressBars.Visibility = Visibility.Collapsed);

            // Stop our timers
            StopTimers();
            MainWindowViewModel.IsExecuting = false;

            if (success)
            {
                // Update our "build result screen" variables
                MainWindowViewModel.BuildResult = "Build Successful.";
                MainWindowViewModel.GlyphsProcessed = data.Glyphs.Count();
                MainWindowViewModel.Class1Count = data.GlyphClass1List.Count();
                MainWindowViewModel.Class2Count = data.GlyphClass1List.First().GlyphClass2List.Count();
            }
            else
            {
                // Display a small failure message in the build results
                MainWindowViewModel.BuildResult = "Build Failed.";

                // Display details in a popup dialog
                string detailedErrorMessage = 
                    "Build Failed.\n\n" +
                    "Check your .ttx file for clues. It's possible your font file is not supported by " +
                    "the .ttx tool, or your font file does not contain a GPOS table. \n\n" +
                    "Check the supported features in the help menu for more details.";

                MessageBox.Show(
                    detailedErrorMessage,
                    "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }

            // If the user tried to close the application while the worker thread
            // was executing, we will have cancelled that operation. We didn't
            // want to close the window until the worker thread was completed.
            // Now that we're done, we want to close the app, like the user 
            // previously requested.
            if (MainWindowViewModel.IsClosing)
                UI(() => Application.Current.Shutdown());
        }

        /// <summary>
        /// Starts our GUI timer and the calculateStatistics timer.
        /// </summary>
        private void StartTimers()
        {
            if (!_timer.IsEnabled)
            {
                _timer.Start();
            }
        }

        /// <summary>
        /// Stops our GUI timer and the calculateStatistics timer.
        /// </summary>
        private void StopTimers()
        {
            if (_timer.IsEnabled)
            {
                _timer.Stop();
            }
        }

        /// <summary>
        /// Executes the provided delegate on the UIDispatcher thread. The code
        /// used to call this function is pretty damn sexy and compact. Check out
        /// these discussions if you want to read more about how to call delegates
        /// using lamda expressions and anonymous functions.
        /// http://stackoverflow.com/questions/4621623/wpf-multithreading-ui-dispatcher-in-mvvm
        /// http://stackoverflow.com/questions/2082615/pass-method-as-parameter-using-c-sharp
        /// </summary>
        /// <param name="delegateAction"></param>
        private void UI(Action delegateAction)
        {
            MainWindowViewModel.UIDispatcher.Invoke(DispatcherPriority.Normal, delegateAction);
        }

        /// <summary>
        /// Timer tick event listener.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            // Increment the number of seconds passed
            MainWindowViewModel.SecondsElapsed++;
        }

        /// <summary>
        /// Button click event listener.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindowViewModel.IsAdornerVisible = false;
        }
    }
}
