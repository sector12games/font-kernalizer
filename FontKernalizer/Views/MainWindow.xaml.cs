﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FontKernalizer.Views
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using FontKernalizer.ViewModels;
    using Sector12Common.Helpers;

    /// <summary>
    /// Class definition for MainWindow.cs.
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowViewModel _viewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            // Create our view model
            _viewModel = new MainWindowViewModel();
            DataContext = _viewModel;

            // The dispatcher is null until after InitializeComponent()
            _viewModel.UIDispatcher = Dispatcher;

            // Retrieve the values the user provided last time they ran
            // the program. This is taken from the WPF Application Settings
            // (right click FontKernalizer -> Properties -> Settings).
            // For more detail, see this discussion:
            // http://stackoverflow.com/questions/396229/wpf-c-where-should-i-be-saving-user-preferences-files
            _viewModel.AfdkoDirectory = Properties.Settings.Default.AfdkoDirectory;
            _viewModel.TargetDirectory = Properties.Settings.Default.TargetDirectory;
        }

        /// <summary>
        /// Pay attention to when the application is terminating.
        /// This will happen if the user clicks the "exit" menu
        /// item, or if the user clicks the "x" in the top right.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            // Code put in by default when visual studio auto-generated
            // this method.
            base.OnClosing(e);

            // Make sure our worker thread is shut down before closing the app.
            // If we don't do this, the worker thread could attempt updating the
            // gui, which will no longer exist. This will obviously cause errors.
            // We don't want to sleep here however - if the worker thread does
            // attempt an Invoke() on the UI thread, it will never get processed,
            // because we are keeping the UI locked up with a sleep. In other words,
            // our worker thread will hang because it's waiting for the UI, and 
            // the UI will hang because it's waiting for the worker. Instead,
            // just tell the worker thread that we want to abort, and cancel the
            // close operation for now. Once the worker thread has aborted and
            // is about to terminate, it will call Application.Current.Shutdown()
            // on the dispatcher thread, and this method will be called once more.
            _viewModel.IsClosing = true;

            // If the worker thread is not finished, don't close the window.
            // Don't sleep either. The worker thread will trigger a shutdown
            // operation once it has finished executing, and this method will
            // be called one more time.
            if (_viewModel.IsExecuting)
                e.Cancel = true;
        }

        /// <summary>
        /// Resets all world builder data. Also resets data within the
        /// ViewModel. It does not reset any data specified by the user 
        /// (checkboxes, save location, etc).
        /// </summary>
        private void Reset()
        {
            // Reset GUI data
            _viewModel.Reset();
        }

        /// <summary>
        /// Displays the supported features dialog.
        /// </summary>
        private void ShowSupportedFeaturesDialog()
        {
            SupportedFeaturesDialog d = new SupportedFeaturesDialog();
            d.Owner = this;
            d.ShowDialog();
        }

        /// <summary>
        /// Displays the about dialog.
        /// </summary>
        private void ShowAboutDialog()
        {
            AboutDialog d = new AboutDialog();
            d.Owner = this;
            d.ShowDialog();
        }

        /// <summary>
        /// Occurs when the "Exit" menu item is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Shut down the application. This will trigger the
            // "OnClosing()" event. 
            Application.Current.Shutdown();
        }

        /// <summary>
        /// Whether or not the "Close" button can be clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            // Allow the user to exit the program at any time
            e.CanExecute = true;
        }

        /// <summary>
        /// Occurs when the "Play" button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlayCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // First, get a reference to the AFDKO directory we will be working 
            // with. Make sure it is a valid directory, contain all of the files
            // we need, and everything is in order. Don't continue if there was
            // a problem with the AFDKO directory.
            DirectoryInfo afdko = GetAndVerifyAfdkoDirectory();
            if (afdko == null)
                return;

            // Next, check to make sure the target directory is in order
            DirectoryInfo target = GetAndVerifyTargetDirectory();
            if (target == null)
                return;

            // Save the values into the WPF Application Settings so that they
            // can be used by default the next time the user runs this program.
            Properties.Settings.Default.AfdkoDirectory = afdko.FullName;
            Properties.Settings.Default.TargetDirectory = target.FullName;
            Properties.Settings.Default.Save();

            // Finally, check to make sure we have only one valid font file
            // in the target directory
            FileInfo font = GetAndVerifyFontFile(target);
            if (font == null)
                return;

            // Now that everything is verified, reset all build variables (timers, etc)
            Reset();

            // Display the progress overlay.
            _viewModel.IsAdornerVisible = true;
            ProgressOverlay control = (ProgressOverlay)ProgressOverlayAdorner.AdornerContent;
            control.MainWindowViewModel = _viewModel;

            // Start processing the request in a different thread
            //Thread thread = new Thread(new ThreadStart(control.StartWorking("test")));
            Thread thread = new Thread(() => control.StartWorking(afdko, target, font));
            thread.Start();
        }

        /// <summary>
        /// Whether or not the "Play" button can be clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlayCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        /// <summary>
        /// MenuItem events bubble up, so I only have to subscribe my entire
        /// menu object to a single MenuItemClick event. All of the menu's 
        /// children will fire events that will be caught here as well.
        /// Just check the incoming event to see which menu item was the
        /// source of the event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            // I implemented commands on all buttons and menu items
            // except the about menu item after I wrote this code.
            // So now, there is only one item being used here. However,
            // I decided to leave this code as is as an example for
            // future projects.
            MenuItem item = e.OriginalSource as MenuItem;
            if (item == null)
                return;

            if (item == AboutMenuItem)
                ShowAboutDialog();

            if (item == SupportedFeaturesMenuItem)
                ShowSupportedFeaturesDialog();
        }

        /// <summary>
        /// Displays a directory chooser popup.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AfdkoDirectoryButton_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.AfdkoDirectory = DialogHelpers.ChooseDirectory(_viewModel.AfdkoDirectory, this);
        }

        /// <summary>
        /// Displays a directory chooser popup. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TargetDirectoryButton_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.TargetDirectory = DialogHelpers.ChooseDirectory(_viewModel.TargetDirectory, this);
        }

        /// <summary>
        /// Ensures that the AFDKO directory exists and contains the ttx.cmd file.
        /// ttx.cmd has too many dependencies to check, so it's possible that the
        /// build could still fail even if this method finds no problems.
        /// </summary>
        /// <returns>A directory that exists and has been verified.</returns>
        private DirectoryInfo GetAndVerifyAfdkoDirectory()
        {
            // Make sure we center message boxes on our form
            MessageBoxHelper.PrepToCenterMessageBoxOnForm(this);

            // Make sure the user has entered some value
            if (string.IsNullOrEmpty(_viewModel.AfdkoDirectory))
            {
                MessageBox.Show(
                    "You must enter a value for \"AFDKO Directory\".", 
                    "Error", 
                    MessageBoxButton.OK, 
                    MessageBoxImage.Error);
                return null;
            }

            // Create the directory info
            DirectoryInfo dir = new DirectoryInfo(_viewModel.AfdkoDirectory);

            // Verify that the directory exists
            if (!dir.Exists)
            {
                MessageBox.Show(
                    "AFDKO directory \"" + dir.FullName + "\" does not exist.",
                    "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return null;
            }

            // Verify that ttx.cmd exists. This file has tons of prerequisites,
            // all of which exist in the AFDKO directories. I can't check all
            // prerequisites, so I'll just check for this one file.
            IEnumerable<FileInfo> afdkoFiles = dir.EnumerateFiles();
            if (afdkoFiles.FirstOrDefault(x => x.Name == "ttx.cmd") == null)
            {
                MessageBox.Show(
                    "AFDKO file \"ttx.cmd\" does not exist.",
                    "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return null;
            }

            // The AFDKO directory checked out. It has everything we need.
            return dir;
        }

        /// <summary>
        /// Ensures that our target directory exists and has a single
        /// font file for us to process. If this check should not pass,
        /// we should display a popup message telling the user to correct
        /// the problem.
        /// </summary>
        /// <returns>A directory that exists and has been verified.</returns>
        private DirectoryInfo GetAndVerifyTargetDirectory()
        {
            // Make sure we center message boxes on our form
            MessageBoxHelper.PrepToCenterMessageBoxOnForm(this);

            // Make sure the user has entered some value
            if (string.IsNullOrEmpty(_viewModel.TargetDirectory))
            {
                MessageBox.Show(
                    "You must enter a value for \"Target Directory\".",
                    "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return null;
            }

            // Create the directory info
            DirectoryInfo dir = new DirectoryInfo(_viewModel.TargetDirectory);

            // Verify that the directory exists
            if (!dir.Exists)
            {
                MessageBox.Show(
                    "Target directory \"" + dir.FullName + "\" does not exist.",
                    "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return null;
            }

            // The target directory checked out. It has everything we need.
            return dir;
        }

        /// <summary>
        /// Ensures that there is only one .ttf or .otf font file in our target
        /// directory. The program will not be allowed to run unless there is only
        /// a single font file to process.
        /// </summary>
        /// <param name="targetDirectory"></param>
        /// <returns>A reference to the .ttf or .otf font file.</returns>
        private FileInfo GetAndVerifyFontFile(DirectoryInfo targetDirectory)
        {
            // Load a list of all valid font files within our target directory
            List<FileInfo> fontFiles = new List<FileInfo>();
            foreach (string ext in Globals.ValidFontExtensions)
            {
                fontFiles.AddRange(targetDirectory.GetFiles(ext, SearchOption.TopDirectoryOnly));
            }

            // Verify that a single font file exists in the target directory
            if (fontFiles.Count == 0)
            {
                MessageBox.Show(
                    "Target directory does not contain a valid font file.",
                    "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return null;
            }

            if (fontFiles.Count > 1)
            {
                MessageBox.Show(
                    "Target directory contains more than one valid font file.",
                    "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return null;
            }

            // Everything checked out. Return the only valid font file.
            return fontFiles.First();
        }

        ///// <summary>
        ///// <para>
        ///// This method is currently not used. I've kept it around as good sample
        ///// code for the future. Sadly, we cannot use c:\Windows\Fonts\.
        ///// </para>
        ///// <para>
        ///// This excludes fonts. c:\Windows\Fonts\ is a customized folder,
        ///// and the OpenFileDialog does not have access to it. You must use
        ///// the FontDialog (ChooseFont()) instead. However, keep in mind that
        ///// ChooseFont() does not support all font types. I believe it only
        ///// supports .ttf files (true type, not open type). When I tested it,
        ///// a .otf file could be shown successfully in the dialog, but when
        ///// I clicked "OK", an error was thrown.
        ///// </para>
        ///// </summary>
        ///// <param name="startingLocation"></param>
        ///// <returns>
        ///// The path to the font file if successful, or the original starting location 
        ///// if unsuccessful (cancel was pressed).
        ///// </returns>
        //private string ChooseFontFile(string startingLocation)
        //{
        //    // Create an OpenFileDialog. We should probably use Microsoft.Win32.OpenFileDialog 
        //    // now instead of System.Windows.Forms.OpenFileDialog. The winforms version
        //    // ends up calling the win32 version anyway. Here's some more detail:
        //    // http://stackoverflow.com/questions/10315188/open-file-dialog-and-select-a-file-using-wpf-controls-and-c
        //    // http://stackoverflow.com/questions/12315908/difference-between-open-savefiledialog-classes-and-their-use-in-a-wpf-form
        //    Microsoft.Win32.OpenFileDialog d = new Microsoft.Win32.OpenFileDialog();
        //    MessageBoxHelper.PrepToCenterMessageBoxOnForm(this);

        //    // Set the initial directory
        //    if (!string.IsNullOrEmpty(startingLocation))
        //        d.InitialDirectory = startingLocation;

        //    // Set the allowed types of files
        //    d.DefaultExt = ".otf";
        //    d.Filter = "Open Type Font (*.otf)|*.otf|True Type Font (*.ttf)|*.ttf";

        //    // Show the dialog. Make sure the dialog is centered when shown. 
        //    Nullable<bool> result = d.ShowDialog();

        //    // Update our textbox to display the file location the user
        //    // just selected in the popup dialog.
        //    if (result == true)
        //        return d.FileName;
        //    else
        //        return startingLocation;
        //}
    }
}
