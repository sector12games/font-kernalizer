﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SupportedFeaturesDialog.xaml.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FontKernalizer.Views
{
    using System;
    using System.Linq;
    using System.Windows;

    /// <summary>
    /// Interaction logic for SupportedFeatures.xaml
    /// </summary>
    public partial class SupportedFeaturesDialog : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SupportedFeaturesDialog"/> class.
        /// </summary>
        public SupportedFeaturesDialog()
        {
            InitializeComponent();
        }
    }
}
