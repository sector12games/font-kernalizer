The Adobe Glyph List files are used for mapping the glyph string name (which appears in our .ttx output)
to an actual unicode character (which can be usable in xna or another game engine).

As of october, 2014, it looks like there are two files, one older, and one newer. I'm not entirely sure
what the differences are, but the way they would be parsed would be slightly different. The two files are
named "Adobe Glyph List" and "Adobe Glyph List For New Fonts".

I've saved both files here for use, and have left some explanatory links below.

http://en.wikipedia.org/wiki/Adobe_Glyph_List
http://partners.adobe.com/public/developer/en/opentype/glyphlist.txt     (AGL   - 2002, old)
http://partners.adobe.com/public/developer/en/opentype/aglfn.txt         (AGLFN - 2006, new)
