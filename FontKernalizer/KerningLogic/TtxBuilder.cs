﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TtxBuilder.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FontKernalizer.KerningLogic
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using Sector12Common.ResourceLoaders;

    /// <summary>
    /// Class definition for TtxBuilder.cs.
    /// </summary>
    public static class TtxBuilder
    {
        /// <summary>
        /// Executes "ttx.cmd" from the AFDKO toolkit.
        /// </summary>
        /// <param name="afdkoDirectory"></param>
        /// <param name="fontFile"></param>
        /// <param name="hideCommandWindow"></param>
        /// <param name="overwrite"></param>
        /// <returns>A reference to the newly created .ttf file.</returns>
        public static FileInfo RunTtx(
            DirectoryInfo afdkoDirectory, 
            FileInfo fontFile, 
            bool hideCommandWindow, 
            bool overwrite)
        {
            // If overwrite is set to true, we want to not only overwrite
            // existing files, but also clean up all old .ttx or serialized
            // kerning data.
            if (overwrite)
                CleanupTargetDirectory(fontFile.Directory);

            // Keep track of all the .ttx files currently in the directory. We
            // need to do this in order to accurately find the ttx file we are
            // about to build. We cannot reliably go by highest counter 
            // (for example LithosPro-Regular#4.ttx) because it's possible for
            // #2, #3, #4 to exist, but not #1 (because I, or someone else deleted 
            // it manually). So #1 will be generated, and we can't use the highest
            // number, because we'll be grabbing #4 - the wrong file. In addition,
            // we can't use the windows time of creation flag for sorting because
            // of a wonderful legacy "feature" (/sarcasm) windows has in place.
            // Basically, if a file is deleted and recreated within a short period
            // of time (15 seconds ~ a few minutes maybe) a cache is searched, and
            // values from the cache are used instead of the new values of the file.
            // In other words, our creation time won't change if we delete and 
            // re-create a file within too short a period of time. Interesting 
            // (and annoying) bug/feature. Here's more detail:
            // http://stackoverflow.com/questions/2109152/unbelievable-strange-file-creation-time-problem
            // http://support.microsoft.com/?kbid=172190
            FileInfo[] ttxFilesPreviouslyExisting = fontFile.Directory.GetFiles("*.ttx");

            // Execute the process and wait for it to complete. This will generate
            // a new .ttx (xml) file for us.
            if (hideCommandWindow)
                RunTtxWithCmdWindowHidden(afdkoDirectory, fontFile);
            else
                RunTtxWithCmdWindowShown(afdkoDirectory, fontFile);

            // Now, look in our target directory (the font's directory) and locate
            // the newly created .ttx file. It's possible other .ttx files are in
            // the directory if we haven't been overwriting, so get the newest file.
            FileInfo[] ttxFiles = fontFile.Directory.GetFiles("*.ttx");
            FileInfo newlyBuiltTtxFile = null;
            foreach (FileInfo f in ttxFiles)
            {
                // We're looking for the only ttx file that didn't exist just
                // before we ran ttx.cmd. Search for the full name in the previously
                // existing list. If it is null, it was the file we just built.
                FileInfo previous = ttxFilesPreviouslyExisting.FirstOrDefault(x => x.FullName.Equals(f.FullName));
                if (previous == null)
                {
                    newlyBuiltTtxFile = f;
                    break;
                }
            }

            // Return a reference to the newly created .ttx file
            return newlyBuiltTtxFile;
        }

        /// <summary>
        /// <para>
        /// We want to show the cmd.exe window while command line tasks are running.
        /// We should also keep the window open at the end, and wait for the user
        /// to press any key, just to ensure he can see all of the output. We need
        /// to use a batch file wrapper in order to do this. All the wrapper does is
        /// ensure a PAUSE is executed at the end of execution.
        /// </para>
        /// <para>
        /// If we use this wrapper with the window hidden, however, the program will hang,
        /// because the PAUSE is waiting for a key press that we can't provide.
        /// </para>
        /// </summary>
        /// <param name="afdkoDirectory"></param>
        /// <param name="fontFile"></param>
        private static void RunTtxWithCmdWindowShown(DirectoryInfo afdkoDirectory, FileInfo fontFile)
        {
            // Load our command wrapper script. Keep in mind that this is a pack uri.
            // In our case, it is a pointer to an embedded resource. This file does 
            // not exist separately on the file system, it is a part of the executable. 
            // So don't expect to be able to get an absolute file system path to 
            // the script.
            Uri commandWrapperUri = new Uri("pack://application:,,,/BatchFiles/CommandWrapper.bat");

            // Now that we have a pack uri to our resource, we need to load it.
            // It is just a batch file, so we will load it as a string, and 
            // start a new process to execute it.
            string commandWrapperContents = WpfResourceLoader.LoadResource<string>(
                commandWrapperUri, 
                ResourceLoaderContentType.Text);

            // Save the embedded command wrapper resource to disk temporarily to
            // allow us to execute it as a batch file. We will use a temporary
            // file name. This is a cool little trick. See this for more details:
            // http://stackoverflow.com/questions/15141010/how-to-run-embedded-batch-file-using-process-start-in-c-sharp
            FileInfo tempFile = CreateTempFile(commandWrapperContents);
            tempFile.MoveTo(Path.ChangeExtension(tempFile.FullName, ".cmd"));

            // Run the process
            Process p = new Process();
            p.StartInfo.FileName = tempFile.FullName;
            p.StartInfo.Arguments = afdkoDirectory.FullName + "\\ttx.cmd " + BuildTtxArguments(fontFile);
            p.EnableRaisingEvents = true;
            p.StartInfo.UseShellExecute = false;
            p.Start();
            p.WaitForExit();

            // Delete the temporary file
            tempFile.Delete();
        }

        /// <summary>
        /// Run the process with the cmd.exe window hidden. We do not use our command
        /// wrapper for this.
        /// </summary>
        /// <param name="afdkoDirectory"></param>
        /// <param name="fontFile"></param>
        private static void RunTtxWithCmdWindowHidden(DirectoryInfo afdkoDirectory, FileInfo fontFile)
        {
            // Run the process
            Process p = new Process();
            p.StartInfo.FileName = afdkoDirectory.FullName + "\\ttx.cmd";
            p.StartInfo.Arguments = BuildTtxArguments(fontFile);
            p.EnableRaisingEvents = true;
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.CreateNoWindow = true;
            p.Start();
            p.WaitForExit();
        }

        /// <summary>
        /// Creates a unique, temporary file on disk and saves text contents
        /// to it. You need to remember to delete this temporary file after use.
        /// </summary>
        /// <param name="contents"></param>
        /// <returns>The FileInfo of the temporary file on disk.</returns>
        private static FileInfo CreateTempFile(string contents)
        {
            // Create a unique temporary file name
            string tempFileName = Path.GetTempFileName();

            // Save the given contents to the temporary file
            File.WriteAllText(tempFileName, contents);

            // Return the FileInfo of the temporary file for further use.
            return new FileInfo(tempFileName);
        }

        /// <summary>
        /// Builds the arguments we want to pass to the ttx.cmd on the command line.
        /// </summary>
        /// <param name="fontFile"></param>
        /// <returns>
        /// the arguments we want to pass to the ttx.cmd on the command line.
        /// </returns>
        private static string BuildTtxArguments(FileInfo fontFile)
        {
            StringBuilder sb = new StringBuilder();

            // Only generate the GPOS table for now. We will need to modify
            // this if we need to use the old KERN table for other fonts in
            // the future.
            sb.Append("-t GPOS ");

            // Output directory. Make this the same directory that the
            // font is located in.
            sb.Append("-d " + fontFile.Directory + " ");

            // Absolute path to font file
            sb.Append(fontFile.FullName);

            return sb.ToString();
        }

        /// <summary>
        /// Deletes old .txt files and serialized .ssbgame files.
        /// </summary>
        /// <param name="targetDirectory"></param>
        private static void CleanupTargetDirectory(DirectoryInfo targetDirectory)
        {
            // Delete any old files with a .ttx file extension
            FileInfo[] ttxFiles = targetDirectory.GetFiles("*.ttx");
            foreach (FileInfo file in ttxFiles)
            {
                file.Delete();
            }

            // Delete any old files with a .ssbgame file extension. This will
            // be our serialized kerning data.
            FileInfo[] serializedDataFiles = targetDirectory.GetFiles("*.ssbgame");
            foreach (FileInfo file in serializedDataFiles)
            {
                file.Delete();
            }
        }
    }
}
