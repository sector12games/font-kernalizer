﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Glyph.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FontKernalizer.KerningLogic
{
    using System;
    using System.Linq;

    /// <summary>
    /// Class definition for Glyph.cs.
    /// </summary>
    public class Glyph
    {
        /// <summary>
        /// Gets or sets the c# unicode character for this glyph.
        /// </summary>
        public char Character { get; set; }

        /// <summary>
        /// <para>
        /// Gets or sets the character in string form. 
        /// </para>
        /// <para>
        /// This basically exists because by default, the c# xml serializer will
        /// serialize the character down into a number, and I won't actually be
        /// able to see the string in my .xml file (I'll just see a number - not
        /// a unicode hex number, but just a number). By also saving the character
        /// as a string, we can prevent the serializer from doing this, and we'll
        /// actually be able to see the character within our xml file as a human
        /// readable character.
        /// </para>
        /// <para>
        /// I may want to remove this in the future - it's not strictly necessary
        /// and it clutters up the code. It certainly makes reading and debugging
        /// the xml file much easier however.
        /// </para>
        /// </summary>
        public string CharacterAsReadableString { get; set; }

        /// <summary>
        /// Gets or sets the name of this string, as it appears in Adobe's generated .ttx
        /// file. For example, a ";" character (semicolon) would be "semicolon".
        /// </summary>
        public string GlyphString { get; set; }

        /// <summary>
        /// Gets or sets the numeric unicode value for this character. This value is 
        /// stored as a string, and will probably only be needed for debugging purposes.
        /// </summary>
        public string UnicodeNumericValue { get; set; }

        /// <summary>
        /// <para>
        /// Gets or sets the coverage offset for this glyph.
        /// </para>
        /// <para>
        /// Not supported yet. Coverage offset is an offset used in fonts
        /// for offsetting entire groups of glyphs. For example, some fonts
        /// might vertically offset all old style roman numerals. At some
        /// point in the future this might be used.
        /// </para>
        /// </summary>
        public int CoverageOffset { get; set; }

        /// <summary>
        /// Gets or sets the class to use when this is the FIRST character in a 
        /// character pair.
        /// </summary>
        public int Class1Index { get; set; }

        /// <summary>
        /// Gets or sets the class to use when this is the SECOND character in a 
        /// character pair.
        /// </summary>
        public int Class2Index { get; set; }
    }
}
