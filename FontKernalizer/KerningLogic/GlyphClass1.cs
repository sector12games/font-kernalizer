﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GlyphClass1.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FontKernalizer.KerningLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Class definition for GlyphClass1.cs
    /// </summary>
    public class GlyphClass1
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GlyphClass1"/> class.
        /// </summary>
        public GlyphClass1()
        {
            GlyphClass2List = new List<GlyphClass2>();
        }

        /// <summary>
        /// Gets or sets this class's order in the Class1 list. 
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// Gets or sets the list of all "Second" classes. These are the classes 
        /// a character will use when it is the SECOND character in a pair. 
        /// </summary>
        public List<GlyphClass2> GlyphClass2List { get; set; }
    }
}
