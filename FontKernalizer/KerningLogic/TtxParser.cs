﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TtxParser.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FontKernalizer.KerningLogic
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Xml;

    /// <summary>
    /// Class definition for TtxParser.cs.
    /// </summary>
    public static class TtxParser
    {
        /// <summary>
        /// Parses and retrieves kerning data from the GPOS table.
        /// </summary>
        /// <param name="ttxFile"></param>
        /// <returns>The kerning data.</returns>
        public static KerningData ParseGposTable(FileInfo ttxFile)
        {
            KerningData data = new KerningData();

            // Open the xml document
            XmlDocument doc = new XmlDocument();
            doc.Load(ttxFile.OpenRead());

            // Now, look for element "ClassDef1". This element contains every 
            // glyph in our font. 
            XmlNode classDef1 = doc.SelectSingleNode(@"//ClassDef1");

            // Determine its format, if available
            XmlAttribute class1Format = classDef1.Attributes["Format"];
            if (class1Format != null)
                data.GlyphClass1Format = class1Format.Value;

            // Iterate over each "ClassDef" element within "ClassDef1" and create 
            // a new Glpyh object for each.
            XmlNodeList classDef1List = classDef1.SelectNodes("ClassDef");
            foreach (XmlNode node in classDef1List)
            {
                // Get the attribute info
                string glyphString = node.Attributes["glyph"].Value;
                int glyphClass1 = int.Parse(node.Attributes["class"].Value);

                // Create a new glyph class
                Glyph g = new Glyph();
                g.GlyphString = glyphString;
                g.Class1Index = glyphClass1;

                // Add the glpyh to our list
                data.Glyphs.Add(g);
            }

            // Now find all "ClassDef2" elements.
            XmlNode classDef2 = doc.SelectSingleNode(@"//ClassDef2");

            // Determine its format, if available
            XmlAttribute class2Format = classDef2.Attributes["Format"];
            if (class2Format != null)
                data.GlyphClass2Format = class2Format.Value;

            // Iterate over each "ClassDef" element within "ClassDef2". Our Glyph
            // objects are already created, so all we need to do is set the class2
            // index.
            XmlNodeList classDef2List = classDef2.SelectNodes("ClassDef");
            foreach (XmlNode node in classDef2List)
            {
                // Get the attribute info
                string glyphString = node.Attributes["glyph"].Value;
                int glyphClass2 = int.Parse(node.Attributes["class"].Value);

                // Find the glyph that already exists in our list
                Glyph g = data.Glyphs.First(x => x.GlyphString.Equals(glyphString));

                // Set the glyph's class 2 value
                g.Class2Index = glyphClass2;
            }

            // Now, build our list of classes. Classes contain all of the kerning 
            // offset information for each glyph. Each glyph in our font has a 
            // "class1". "Class1" means when this letter is the FIRST character 
            // in a pair of characters. Each "Class1" has a list of "Class2". 
            // "Class2" means characters that are SECOND in a pair of characters. 
            // Classes can be shared by many glyphs.
            XmlNodeList class1Record = doc.SelectNodes(@"//Class1Record");
            foreach (XmlNode c1Node in class1Record)
            {
                // Each class has an index. This index is it's identifier. Every
                // glpyh will have a "class" attribute that corresponds to this
                // index. So a glpyh with "class=4" means classIndex="4".
                int c1Index = int.Parse(c1Node.Attributes["index"].Value);

                // Create a new GlyphClass1 object. GlyphClass1 is for glyphs
                // that are the FIRST glyph in a pair.
                GlyphClass1 gc1 = new GlyphClass1();
                gc1.Index = c1Index;

                // Add gc1 to our list
                data.GlyphClass1List.Add(gc1);

                // Each GlyphClass1 has a list of GlyphClass2. GlyphClass2 is for
                // glyphs that are the SECOND glyph in the pair.
                XmlNodeList class2Record = c1Node.SelectNodes(@"Class2Record");
                foreach (XmlNode c2Node in class2Record)
                {
                    // Each Class2 also has an index
                    int c2Index = int.Parse(c2Node.Attributes["index"].Value);

                    // Get the kerning data for this pair. Kerning data only
                    // effects the second character in a pair. That's why we
                    // have two classes - GlyphClass1 and GlyphClass2. Only
                    // GlyphClass2 contains kerning data.
                    int xAdvance = int.Parse(c2Node.SelectSingleNode("Value1").Attributes["XAdvance"].Value);

                    // Create a new GlyphClass2 object. GlyphClass2 is for glyphs
                    // that are the SECOND glyph in a pair.
                    GlyphClass2 gc2 = new GlyphClass2();
                    gc2.Index = c2Index;
                    gc2.XAdvance = xAdvance;

                    // Add the new class2 to our current class1
                    gc1.GlyphClass2List.Add(gc2);
                }
            }

            return data;
        }

        /// <summary>
        /// Parses and retrieves kerning data from the old-school KERN table.
        /// </summary>
        /// <returns>The kerning data.</returns>
        public static KerningData ParseKernTable()
        {
            // Currently unsupported. KERN table is used on older fonts.
            KerningData data = new KerningData();
            return data;
        }
    }
}
