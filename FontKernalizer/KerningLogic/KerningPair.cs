﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KerningPair.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FontKernalizer.KerningLogic
{
    using System;
    using System.Linq;

    /// <summary>
    /// Class definition for KerningPair.cs.
    /// </summary>
    public class KerningPair
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KerningPair"/> class.
        /// </summary>
        /// <param name="firstSymbol"></param>
        /// <param name="secondSymbol"></param>
        public KerningPair(Glyph firstSymbol, Glyph secondSymbol)
        {
            FirstSymbol = firstSymbol;
            SecondSymbol = secondSymbol;
        }

        /// <summary>
        /// Gets or sets the first character or symbol in this pair.
        /// </summary>
        public Glyph FirstSymbol { get; set; }

        /// <summary>
        /// Gets or sets the second character or symbol in this pair.
        /// </summary>
        public Glyph SecondSymbol { get; set; }

        /// <summary>
        /// Gets or sets how much to horizontally adjust the second character in 
        /// this kerning pair. This value could be positive or negative.
        /// </summary>
        public int XAdvance { get; set; }
    }
}
