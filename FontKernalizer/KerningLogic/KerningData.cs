﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KerningData.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FontKernalizer.KerningLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Class definition for KerningData.cs.
    /// </summary>
    public class KerningData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KerningData"/> class.
        /// </summary>
        public KerningData()
        {
            Glyphs = new List<Glyph>();
            GlyphClass1List = new List<GlyphClass1>();
        }

        /// <summary>
        /// Gets or sets our master list of glyphs. Make sure that it's read only
        /// from outside of this class.
        /// </summary>
        public List<Glyph> Glyphs { get; set; }

        /// <summary>
        /// Gets or sets the list of all "First" classes. These are the classes a 
        /// character will use when it is the FIRST character in a pair. Each class1 has
        /// a list of class2s that the second character in the pair can choose from.
        /// </summary>
        public List<GlyphClass1> GlyphClass1List { get; set; }

        /// <summary>
        /// <para>
        /// Gets or sets the format of GlyphClass1.
        /// </para>
        /// <para>
        /// Currently unused, but is populated from the .ttx file. Could be
        /// used in the future.
        /// </para>
        /// </summary>
        public string GlyphClass1Format { get; set; }

        /// <summary>
        /// <para>
        /// Gets or sets the format of GlyphClass2.
        /// </para>
        /// <para>
        /// Currently unused, but is populated from the .ttx file. Could be
        /// used in the future.
        /// </para>
        /// </summary>
        public string GlyphClass2Format { get; set; }

        /// <summary>
        /// Gets kerning data for a pair of characters.
        /// </summary>
        /// <param name="firstChar"></param>
        /// <param name="secondChar"></param>
        /// <returns>The kerning data for a pair of characters.</returns>
        public KerningPair GetKerningPair(char firstChar, char secondChar)
        {
            // First, find our glyphs
            Glyph g1 = Glyphs.First(x => x.Character == firstChar);
            Glyph g2 = Glyphs.First(x => x.Character == secondChar);

            // Find our classes. Our first glyph will use class1. Our
            // second glyph will use class2. Remember that each class1 
            // will have a list of every available class2.
            GlyphClass1 gc1 = GlyphClass1List[g1.Class1Index];
            GlyphClass2 gc2 = gc1.GlyphClass2List[g2.Class2Index];

            // Create a more friendly class to pass to the consumer
            KerningPair p = new KerningPair(g1, g2);
            p.XAdvance = gc2.XAdvance;

            // Return
            return p;
        }
    }
}
