﻿// -----------------------------------------------------------------------
// <copyright file="AdobeGlyphStringToUnicodeMapper.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace FontKernalizer.KerningLogic
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using Sector12Common.ResourceLoaders;

    /// <summary>
    /// Class definition for AdobeGlyphStringToUnicodeMapper.cs
    /// </summary>
    public static class AdobeGlyphStringToUnicodeMapper
    {
        /// <summary>
        /// <para>
        /// Maps adobe glyph strings (provided by the .ttx dump) to unicode characters,
        /// which will be usable by xna or another game engine. The mapping is done
        /// using the Adobe Glyph List, from 2002.
        /// </para>
        /// <para>
        /// In general, this method should be preferred for older fonts.
        /// </para>
        /// </summary>
        /// <param name="data"></param>
        public static void MapAGL(KerningData data)
        {
            // Load a string from file
            Uri uri = new Uri("pack://application:,,,/Content/AdobeGlyphLists/agl.txt");
            string agl = WpfResourceLoader.LoadResource<string>(uri, ResourceLoaderContentType.Text);

            // Parse the document, and create a symbol to unicode value mapping
            Dictionary<string, string> mapping = new Dictionary<string, string>();
            using (StringReader reader = new StringReader(agl))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    // If the line starts with a '#' it is a comment and should be ignored
                    if (string.IsNullOrEmpty(line) || line.StartsWith("#"))
                        continue;

                    // Each line is semi-colon delimited, with two segments. Split it.
                    string[] split = line.Split(';');

                    // The first segment is the adobe glyph string, and the second 
                    // segment is the unicode character. Add them to the dictionary.
                    string adobeGlyphString = split[0];
                    string unicodeNumericValue = split[1];

                    // Add them to the dictionary
                    mapping.Add(adobeGlyphString, unicodeNumericValue);
                }
            }

            // Add our mapping values to the kerning data
            AddMappingValuesToKerningData(data, mapping);
        }

        /// <summary>
        /// <para>
        /// Maps adobe glyph strings (provided by the .ttx dump) to unicode characters,
        /// which will be usable by xna or another game engine. The mapping is done
        /// using the Adobe Glyph List For New Fonts, from 2006.
        /// </para>
        /// <para>
        /// In general, this method should be preferred for newer fonts.
        /// </para>
        /// </summary>
        /// <param name="data"></param>
        public static void MapAGLFN(KerningData data)
        {
            // Load a string from file
            Uri uri = new Uri("pack://application:,,,/Content/AdobeGlyphLists/aglfn.txt");
            string agl = WpfResourceLoader.LoadResource<string>(uri, ResourceLoaderContentType.Text);

            // Parse the document, and create a symbol to unicode value mapping
            Dictionary<string, string> mapping = new Dictionary<string, string>();
            using (StringReader reader = new StringReader(agl))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    // If the line starts with a '#' it is a comment and should be ignored
                    if (string.IsNullOrEmpty(line) || line.StartsWith("#"))
                        continue;

                    // Each line is semi-colon delimited, with two segments. Split it.
                    string[] split = line.Split(';');

                    // This is where things are different from the old AGL. The unicode
                    // value comes first, follwed by the adobe glyph name, followed by
                    // a longer, more descriptive name. We only care about the first
                    // two.
                    string unicodeNumericValue = split[0];
                    string adobeGlyphString = split[1];

                    // Add them to the dictionary
                    mapping.Add(adobeGlyphString, unicodeNumericValue);
                }
            }

            // Add our mapping values to the kerning data
            AddMappingValuesToKerningData(data, mapping);
        }

        /// <summary>
        /// Iterates over each glyph in our kerning data and adds the unicode mapping 
        /// values we retrieved from one of our adobe glyph list files. If no mapping
        /// value was found, it's possible that the adobe glyph string is already
        /// in unicode form, or there could be other special cases. We will check for
        /// those as well.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="glyphStringToUnicodeMapping"></param>
        private static void AddMappingValuesToKerningData(KerningData data, Dictionary<string, string> glyphStringToUnicodeMapping)
        {
            // For each glyph in our kerning data, add both the unicode numeric value
            // and the character itself.
            foreach (Glyph glyph in data.Glyphs)
            {
                // Determine the glyph string for this glyph
                string baseGlyphName = glyph.GlyphString;

                // There are various special "modifiers" that font designers can use 
                // when assigning style classes. In general, I think the name for these
                // are "stylistic alternates". For example, they allow you to change the
                // font color or size or shape when cetain characters appear next to
                // each other, or certain characters are at the beginning or end of a
                // word, etc. There's tons of different modifiers in Lithos-Pro even.
                // For example, I've seen .sc, .alt, .fitted, .oldstyle... the list goes
                // on. So, for example, for a single character, such as the letter 'A',
                // you may have several entries - 'A', 'A.sc', 'A.oldstyle', etc. 
                // Essentially, these are all the same letter, but they use different
                // classes. We should ignore the modifier suffix and just assign the
                // unicode character value to all of them. Keep in mind that these
                // styleistic suffixes will almost surely never be supported by any game
                // I plan on making, and certainly not in xna. From what I've read, 
                // even Adobe Illustrator doesn't support certain stylistic alternates.
                // However, they were added to the list by our .ttx dump, so I'll keep
                // them for completeness.
                if (baseGlyphName.Contains('.'))
                    baseGlyphName = baseGlyphName.Substring(0, baseGlyphName.IndexOf('.'));

                // Attempt to find the unicode value in the mapping we created 
                string unicodeValue = TryAGLMapping(baseGlyphName, glyphStringToUnicodeMapping);

                // If unsuccessful, check to see if the adobe glyph string was already 
                // in unicode format
                if (string.IsNullOrEmpty(unicodeValue))
                    unicodeValue = TryAlreadyUnicode(baseGlyphName);

                // Save the unicode value if one was found, otherwise, write an error message
                if (!string.IsNullOrEmpty(unicodeValue))
                {
                    // Store the actual unicode numeric value as a string
                    glyph.UnicodeNumericValue = unicodeValue;

                    // Convert the unicode numeric value (eg. 0041) to a character
                    // http://stackoverflow.com/questions/8400050/converting-hex-string-back-to-char
                    glyph.Character = (char)int.Parse(unicodeValue, NumberStyles.AllowHexSpecifier);

                    // Also store the character as a string, which will prevent the xml 
                    // serializer from changing the character into a number (the character
                    // won't actually show up in your xml file, you'll see a number 
                    // instead).
                    glyph.CharacterAsReadableString = glyph.Character.ToString();
                }
                else
                {
                    // Write an error message
                    glyph.UnicodeNumericValue = "Error - Unicode mapping not found.";
                }   
            }
        }

        /// <summary>
        /// Tries to find the glyph's unicode character by looking at our adobe glyph
        /// to unicode mapping.
        /// </summary>
        /// <param name="baseGlyphName"></param>
        /// <param name="glyphStringToUnicodeMapping"></param>
        /// <returns>Either the glyph's unicode value, or null if not found.</returns>
        private static string TryAGLMapping(string baseGlyphName, Dictionary<string, string> glyphStringToUnicodeMapping)
        {
            // Attempt to find the mapping between the adobe glyph string 
            // (human readable) and its corresponding unicode value.
            string unicodeNumericValue;
            glyphStringToUnicodeMapping.TryGetValue(baseGlyphName, out unicodeNumericValue);

            // Return either our unicode value, or null if not found
            return unicodeNumericValue;
        }

        /// <summary>
        /// Checks to see if the adobe glyph string was already in unicode format.
        /// </summary>
        /// <param name="baseGlyphName"></param>
        /// <returns>Either the glyph's unicode value, or null if not found.</returns>
        private static string TryAlreadyUnicode(string baseGlyphName)
        {
            // If the mapping was null, it's possible that the glyph string was 
            // already in unicode form after the .ttx dump. Not every unicode
            // value has a human-readable adobe glyph string. In this case, the
            // .ttx dump convention seems to be 'uni' prefixing the unicode value. 
            // For example, 'uni0122'. There's also a few cases where you'll see
            // something like this: 'space_uni0326'. Because of this, just go
            // ahead and search for 'uni' within the string, not just as a prefix.
            if (!baseGlyphName.Contains("uni"))
                return null;

            // Grab everything after "uni"
            int index = baseGlyphName.IndexOf("uni", StringComparison.CurrentCultureIgnoreCase) + 3;

            // Return the unicode numeric value
            return baseGlyphName.Substring(index);
        }
    }
}
