﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GlyphClass2.cs" company="Sector12">
//   Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FontKernalizer.KerningLogic
{
    using System;
    using System.Linq;

    /// <summary>
    /// Class definition for GlyphClass2.cs.
    /// </summary>
    public class GlyphClass2
    {
        /// <summary>
        /// Gets or sets this class's order in the Class2 list. 
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// Gets or sets how much to horizontally adjust glyphs that use this class.
        /// This value could be positive or negative.
        /// </summary>
        public int XAdvance { get; set; }
    }
}
